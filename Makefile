###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Max Mochizuki <maxmm@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   1 February 2021
###############################################################################

CC 	= gcc
TARGET 	= animalfarm
CFLAGS  = -g 

HDR = $(SRC:.c=.h)
OBJ = $(SRC:.c=.o)

SRC= main.c cat.c animals.c

all: $(TARGET)

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

main.o: cat.h animals.h
	$(CC) $(CFLAGS) -c main.c

animalfarm: cat.o animals.o main.o
	$(CC) $(CFLAGS) -o animalfarm cat.o animals.o main.o

clean:
	rm -f $(OBJ) $(TARGET)
