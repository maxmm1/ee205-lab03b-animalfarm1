///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Max Mochizuki <maxmm@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   1 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"


/// Decode the enum Gender into strings for printf()
char* genderName (enum Gender gender) {
   switch(gender){
      case MALE: return "Male";
      break;
      case FEMALE: return "Female";
      break;
   default: return "ERROR";
   }
};


/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   switch(color){
      case BLACK: return "Black";
      break;
      case WHITE: return "White";
      break;
      case RED: return "Red";
      break;
      case BLUE: return "Blue";
      break;
      case PINK: return "Pink";
      break;
   default: return "ERROR";
   }
};


/// Decode the enum catBreeds into strings for printf()
char* breedsName (enum CatBreeds breed) {
   switch(breed){
      case MAIN_COON: return "Main Coon";
      break;
      case MANX: return "Manx";
      break;
      case SHORTHAIR: return "Shorthair";
      break;
      case PERSIAN: return "Persian";
      break;
      case SPHYNX: return "Sphynx";
      break;
   default: return "ERROR";
   }
};


/// Decode the boolean isFixed into strings for printf()
char* isfixedName (bool isFixed) {
   if(isFixed = true)
      return "Yes";
   else
      return "No";  
};


